import React, {Component} from 'react';
import {StyleSheet,AppRegistry, Text, View} from 'react-native';
import { createStackNavigator, createAppContainer} from 'react-navigation';
import Home from './src/views/home/Home';
import Login from './src/views/login/Login';
import Regist from './src/views/regist/Regist';
import ForgotPassword from './src/views/login/ForgotPassword';
import Profile from './src/views/home/Profile';
import Transaction from './src/views/home/Transaction';
import Portofolio from './src/views/home/Portofolio';



var AppNavigator = createStackNavigator({
  Login:Login,
  Regist:Regist,
  Home:Home,
  ForgotPassword:ForgotPassword,
  Profile:Profile,
  Transaction:Transaction,
  Portofolio:Portofolio
},
{
 initialRouteName:'Login',
 headerMode:'float',
 headerTransitionPreset:'fade-in-place', 
})
export default createAppContainer(AppNavigator);