import React, {Component} from 'react';
import {StyleSheet,AppRegistry,Image,ScrollView} from 'react-native';
import { Button,Container,Content,Text,View } from 'native-base';
import {Row,Col,Grid} from 'react-native-easy-grid';

export default class Home extends React.Component {
  static navigationOptions = {
    header: null,
  };
  render() {
    return (
      <ScrollView
          style={styles.contentContainer}
          showsHorizontalScrollIndicator={false}
          showsVerticalScrollIndicator={false}
        >
        <Grid>
          <Col size={0.25} ></Col>
          <Col size={9.5} >
            <View style={styles.shadow2} >
              <Text style={{color:'white'}} >RP. 0</Text>
              <Text style={{color:'white'}} >Total Investasi</Text>
            </View>

            <View style={styles.shadow2} >
              <Text style={{marginBottom:10,fontSize:20,marginLeft:10,color:'white'}} >Produk Kami</Text>

              <View style={styles.shadow} >
                <Image source={{uri: 'https://api.paytren-am.co.id:9091/asset/img/3.png'}} style={{width:'100%',height:450}}
                />
                <View style={{marginLeft:10,marginTop:10}} >
                  <Text>Reksa Dana Syariah PAM Syariah Campuran Dana Daqu.Prospektus</Text>
                  <Text>Reksadana Campuran Berbasis Syariah</Text>
                  <Text>NAB/Unit per Tanggal 25-10-2018 Rp 1,007.87</Text>
                </View>
                <Button style={styles.btn} ><Text>Beli</Text></Button>
                <Image source={require('../../../public/assets/images/charss.png')} style={{width:'93%',    marginTop:10,marginLeft:10}} />
              </View>

              <View style={styles.shadow} >
                <Image source={{uri: 'https://api.paytren-am.co.id:9091/asset/img/3.png'}} style={{width:'100%',height:450}}
                />
                <View style={{marginLeft:10,marginTop:10}} >
                  <Text>Reksa Dana Syariah PAM Syariah Campuran Dana Daqu.Prospektus</Text>
                  <Text>Reksadana Campuran Berbasis Syariah</Text>
                  <Text>NAB/Unit per Tanggal 25-10-2018 Rp 1,007.87</Text>
                </View>
                <Button style={styles.btn} ><Text>Beli</Text></Button>
                <Image source={require('../../../public/assets/images/charss.png')} style={{width:'93%',    marginTop:10,marginLeft:10}} />
              </View>

              <View style={styles.shadow} >
                <Image source={{uri: 'https://api.paytren-am.co.id:9091/asset/img/3.png'}} style={{width:'100%',height:450}}
                />
                <View style={{marginLeft:10,marginTop:10}} >
                  <Text>Reksa Dana Syariah PAM Syariah Campuran Dana Daqu.Prospektus</Text>
                  <Text>Reksadana Campuran Berbasis Syariah</Text>
                  <Text>NAB/Unit per Tanggal 25-10-2018 Rp 1,007.87</Text>
                </View>
                <Button style={styles.btn} ><Text>Beli</Text></Button>
                <Image source={require('../../../public/assets/images/charss.png')} style={{width:'93%',    marginTop:10,marginLeft:10}} />
              </View>

            </View>
          </Col>
          <Col size={0.25} ></Col>
        </Grid>
      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
  btn:{
    backgroundColor:'#598fe5',
    justifyContent:'center',
    width:'94%',
    borderRadius:5,
    marginTop:30,
    marginBottom:20,
    marginLeft:10
  },
  shadow:{
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.5,
    shadowRadius: 3.84,
    elevation: 7,
    backgroundColor:'white',
    marginBottom:30,
    opacity:1
  },
  shadow2:{
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.5,
    shadowRadius: 3.84,
    elevation: 5,
    backgroundColor: 'rgba(162,136,128, 1)',
    width:'100%',
    padding:10,
    marginTop:10,
    marginBottom:40
  }
});

  AppRegistry.registerComponent('propstate',()=>Home);
  