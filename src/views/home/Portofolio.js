import React, {Component} from 'react';
import {StyleSheet,AppRegistry, Text, View,Image} from 'react-native';

export default class Portofolio extends Component {
 
    render() {
      return (
        <View style={styles.container}>
          <Image source={require('../../../public/logo.png')} />
          <Text style={styles.welcome}>Masuk aaaaaa Personal Area</Text>
          <Text style={styles.silahkan} >Silahkan masukkan email dan sandi untuk masuk ke Personal Area. Anda dapat mengelola akun Anda dari Personal Area.</Text>
        </View>
      );
    }
  }
  
  const styles = StyleSheet.create({
    container: {
      flex: 1,
      justifyContent: 'center',
      alignItems: 'center',
      backgroundColor: '#F5FCFF',
    },
    welcome: {
      fontSize: 20,
      textAlign: 'center',
      margin: 10,
    },
    silahkan: {
      fontSize: 15,
      textAlign: 'center',
      margin: 10,
    }
  });

  AppRegistry.registerComponent('propstate',()=>Portofolio);
  