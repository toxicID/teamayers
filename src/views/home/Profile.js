import React, {Component} from 'react';
import {StyleSheet,AppRegistry,Image,ScrollView} from 'react-native';
import { Button,Container,Content,Text,View,Icon,Accordion } from 'native-base';
import {Row,Col,Grid} from 'react-native-easy-grid';

export default class Profile extends React.Component {
  static navigationOptions = {
    header: null,
  };
  render() {
    return (
      <ScrollView
          showsHorizontalScrollIndicator={false}
          showsVerticalScrollIndicator={false}
        >
        <Grid>
          <Col size={0.5} ></Col>
          <Col size={9} >
            <View style={styles.shadow2} >
              <Text>Profil Saya (Individual)</Text>
              <Icon name='create' />
            </View>

            <View style={styles.shadow2} > 
              <Icon name="person">
                <Text style={{fontSize:25}} >Informasi Data Diri</Text>
              </Icon>

              <Text>Nama lengkap:</Text>
              <Text>Andhika</Text>

              <Text>Kewarganegaraan :</Text>
              <Text>Indonesia</Text>

              <Text>Provinsi :</Text>
              <Text>Jakarta Raya</Text>

              <Text>Kota :</Text>
              <Text>Jakarta Selatan</Text>

              <Text>Kecamatan :</Text>
              <Text>Kebagusan</Text>

              <Text>Alamat :</Text>
              <Text>Jalan Kebagusan</Text>

              <Text>No HP :</Text>
              <Text>08123456789</Text>

              <Text>Pendidikan :</Text>
              <Text>S1</Text>

              <Text>Foto KTP</Text>
              <Image/>

              <Text>Foto NPWP</Text>
              <Image/>

              <Icon name='briefcase' style={{marginTop:20}} >
                <Text style={{fontSize:25}} >Informasi Data Pekerjaan</Text>
              </Icon>

              <Text>Pekerjaan :</Text>
              <Text>Karyawan Swasta</Text>

              <Text>Sumber Dana :</Text>
              <Text>Gaji</Text>

              <Text>Penghasilan Pertahun :</Text>
              <Text>> 10 - 50 juta / tahun</Text>

              <Icon name='stats' style={{marginTop:20}}>
                <Text style={{fontSize:25}} >Informasi Data Bank</Text>
              </Icon>
              <Text>Nama Bank 1</Text>
              <Text>BCA</Text>

              <Text>Nomor Rekening :</Text>
              <Text>123456789</Text>

              <Text>Cabang :</Text>
              <Text>Jakarta Pusat</Text>

              <Text>Rekening Atas Nama :</Text>
              <Text>Andhika</Text>

            </View>
          </Col>
          <Col size={0.5} ></Col>
        </Grid>
      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
  btn:{
    backgroundColor:'#598fe5',
    justifyContent:'center',
    width:'94%',
    borderRadius:5,
    marginTop:30,
    marginBottom:20,
    marginLeft:10
  },
  shadow2:{
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.5,
    shadowRadius: 3.84,
    elevation: 5,
    backgroundColor: 'white',
    width:'100%',
    padding:10,
    marginTop:10,
    marginBottom:40
  }
});

  AppRegistry.registerComponent('propstate',()=>Profile);
  