import React, {Component} from 'react';
import {StyleSheet,AppRegistry, Text, View,Image} from 'react-native';
import {Button} from 'native-base';

  export default class Step3 extends Component{

    nextPreprocess=()=>{
      
        this.props.saveState(3,{key:'value'})
        this.props.nextFn()
            
      }
      
      previousPreprocess=()=>{
            
        this.props.prevFn()
            
      }

    render() {
        return (
          <View>     
            <Text>ini step 3 ya...</Text>
            <Button onPress={this.nextPreprocess} ><Text>Next</Text></Button>
            <Button onPress={this.previousPreprocess} ><Text>Prev</Text></Button>
          </View>
        );
      }
    }

