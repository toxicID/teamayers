import React, {Component} from 'react';
import {StyleSheet,AppRegistry, Text, View,Image} from 'react-native';
import {Button} from 'native-base';
import MultiStep from 'react-native-multistep-wizard'
import Step1 from './Step1'
import Step2 from './Step2'
import Step3 from './Step3'

  
          
  export default class Regist extends Component {
    static navigationOptions = {
      header: null,
    };
    constructor(props){
      super(props);
      this.state={
        steps:[
        {name: 'Step1', component: <Step1/>},
        {name: 'Step2', component: <Step2/>},
        {name: 'Step3', component: <Step3/>},
      ],
      }
    }

    finish=(wizardState)=>{
         
          }

      render(){
        const { steps } = this.state
          return(
              <View style={styles.container}>
                <MultiStep steps={steps} onFinish={this.finish}/>
              </View>
          )
      }
    }
  
  const styles = StyleSheet.create({
    container: {
      flex: 1,
      justifyContent: 'center',
      alignItems: 'center',
      backgroundColor: '#F5FCFF',
      },
    }
  );

  AppRegistry.registerComponent('propstate',()=>Regist);
  