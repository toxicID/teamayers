import React, {Component} from 'react';
import {StyleSheet,AppRegistry, Text, View,Image} from 'react-native';
import {Button} from 'native-base';

  export default class Step2 extends Component{

  nextPreprocess=()=>{
      
    this.props.saveState(2,{key:'value'})
    this.props.nextFn()
        
  }
  
  previousPreprocess=()=>{
        
    this.props.prevFn()
        
  }

    render() {
        return (
          <View>     
            <Text>ini step 2 ya...</Text>
            <Button onPress={this.nextPreprocess} ><Text>Next</Text></Button>
            <Button onPress={this.previousPreprocess} ><Text>Prev</Text></Button>
          </View>
        );
      }
    }

