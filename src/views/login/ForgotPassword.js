import React, {Component} from 'react';
import {StyleSheet,AppRegistry, Text, View,Image} from 'react-native';

export default class ForgotPassword extends Component {
 
    render() {
      return (
        <View style={styles.container}>
          <Text style={styles.silahkan} >ini forgot pass ya...</Text>
        </View>
      );
    }
  }
  
  const styles = StyleSheet.create({
    container: {
      flex: 1,
      justifyContent: 'center',
      alignItems: 'center',
      backgroundColor: '#F5FCFF',
    },
    welcome: {
      fontSize: 20,
      textAlign: 'center',
      margin: 10,
    },
    silahkan: {
      fontSize: 15,
      textAlign: 'center',
      margin: 10,
    }
  });

  AppRegistry.registerComponent('propstate',()=>ForgotPassword);
  